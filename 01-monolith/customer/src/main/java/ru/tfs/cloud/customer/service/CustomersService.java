package ru.tfs.cloud.customer.service;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tfs.cloud.customer.domain.Customer;
import ru.tfs.cloud.customer.repository.CustomersRepository;

@Service
@RequiredArgsConstructor
public class CustomersService {
    private final CustomersRepository customersRepository;

    public List<Customer> findAll() {
        return customersRepository.findAll();
    }

    public Optional<Customer> findById(long id) {
        return customersRepository.findById(id);
    }
}
