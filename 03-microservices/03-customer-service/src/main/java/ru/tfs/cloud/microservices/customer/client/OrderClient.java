package ru.tfs.cloud.microservices.customer.client;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.tfs.cloud.microservices.customer.dto.OrderDto;

@FeignClient(name = "order")
public interface OrderClient {

    @GetMapping("/api/order/{customerId}")
    List<OrderDto> getOrders(@PathVariable long customerId);
}
