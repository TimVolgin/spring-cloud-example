package ru.tfs.cloud.microservices.order;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import ru.tfs.cloud.microservices.order.domain.Order;
import ru.tfs.cloud.microservices.order.repository.OrdersRepository;

@SpringBootApplication
@EnableDiscoveryClient
@Slf4j
public class OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderApplication.class, args);
	}

	@Bean
	public RouterFunction<ServerResponse> routes(OrdersRepository repository) {
		return RouterFunctions.route()
			.GET("api/order/{customerId}", RequestPredicates.accept(APPLICATION_JSON),
				 serverRequest -> {
					 long customerId = Long.parseLong(serverRequest.pathVariable("customerId"));
					 log.info("get orders by customer id {}", customerId);
					 Flux<Order> orders = repository.findByCustomerId(customerId);
					 return ok().contentType(APPLICATION_JSON).body(orders, Order.class);
				 })
			.build();
	}

}
