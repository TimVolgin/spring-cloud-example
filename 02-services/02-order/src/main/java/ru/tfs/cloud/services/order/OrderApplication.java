package ru.tfs.cloud.services.order;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.tfs.cloud.services.order.domain.Order;
import ru.tfs.cloud.services.order.repository.OrdersRepository;

@SpringBootApplication
public class OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderApplication.class, args);
	}

	@Bean
	public RouterFunction<ServerResponse> routes(OrdersRepository repository) {
		return RouterFunctions.route()
			.GET("api/orders/{customerId}", RequestPredicates.accept(APPLICATION_JSON),
				 serverRequest -> {
					 long customerId = Long.parseLong(serverRequest.pathVariable("customerId"));
					 return ok().contentType(APPLICATION_JSON)
						 .body(repository.findByCustomerId(customerId), Order.class);
				 })
			.build();
	}

}
