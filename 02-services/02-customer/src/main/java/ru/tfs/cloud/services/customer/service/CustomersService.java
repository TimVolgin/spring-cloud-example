package ru.tfs.cloud.services.customer.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.tfs.cloud.services.customer.domain.Customer;
import ru.tfs.cloud.services.customer.dto.CustomerDto;
import ru.tfs.cloud.services.customer.dto.OrderDto;
import ru.tfs.cloud.services.customer.repository.CustomersRepository;

@Service
@RequiredArgsConstructor
public class CustomersService {
    private static final String ORDERS_URL = "http://localhost:8082/api/orders/";

    private final CustomersRepository customersRepository;
    private final RestTemplate restTemplate = new RestTemplate();

    public List<CustomerDto> getCustomers() {
        return customersRepository.findAll()
            .stream()
            .map(this::mapToDto)
            .collect(Collectors.toList());
    }

    private CustomerDto mapToDto(Customer customer) {
        OrderDto[] ordersArray = restTemplate.getForObject(ORDERS_URL + customer.getId(), OrderDto[].class);
        if (ordersArray == null) {
            return new CustomerDto(customer);
        }
        return new CustomerDto(customer, Arrays.asList(ordersArray));
    }

    public Optional<CustomerDto> getCustomer(long id) {
        return customersRepository.findById(id).map(this::mapToDto);
    }

}
